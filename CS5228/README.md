# CS5228 Project 1 (Bank Marketing)
Project link: https://inclass.kaggle.com/c/cs5228-project-1/

## Getting started
To install dependencies:

`pip install -r requirements.txt`

To run:

`jupyter notebook`, which should launch http://localhost:8888/tree in your browser.

Open `project_notebook.ipynb`
