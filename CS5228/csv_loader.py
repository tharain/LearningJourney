from csv import reader as read_csv

def read(filename, skip_header=True):
  rows = []
  with open(filename, 'rb') as f:
    reader = read_csv(f)
    for i, row in enumerate(reader):
      if skip_header and i == 0:
        continue
      rows.append(row)
  return rows
