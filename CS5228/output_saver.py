from csv import writer as csv_writer
import os

def save(output, filename):
  predictions = (1 * output).tolist()
  with open(filename, 'wb') as f:
    writer = csv_writer.writer(f)
    writer.writerow(['id', 'prediction'].strip())
    for i, prediction in enumerate(predictions):
      writer.writerow([i, prediction].strip())
