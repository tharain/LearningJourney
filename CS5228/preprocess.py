import numpy as np

NUMERIC_COLUMNS = [11, 13, 16, 17, 18, 19, 20]
#NUMERIC_COLUMNS = [1, 11, 12, 13, 14, 16, 17, 18, 19, 20]
CATEGORICAL_COLUMNS = {
#  2: ['admin.','blue-collar','entrepreneur','housemaid','management','retired',
#      'self-employed','services','student','technician','unemployed','unknown'],
#  3: ['divorced','married','single','unknown'],
  4: ['basic.4y','basic.6y','basic.9y','high.school','illiterate',
      'professional.course','university.degree','unknown'],
  5: ['no','yes','unknown'],
#  6: ['no','yes','unknown'],
#  7: ['no','yes','unknown'],
  8: ['cellular','telephone'],
  9: ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'],
#  10: ['mon','tue','wed','thu','fri'],
  15: ['failure','nonexistent','success']
}

class Preprocessor:
  def process(self, data, is_train=True):
    """
    Processes the input data loaded from the csv file. Returns a 2d numpy array
    of N x D (D is the dimensionality of each input row)

    data:     N Rows from the csv file
    is_train: Indicates if the data is training set data or test set data
    """
    processed_x = []
    processed_y = []
    for row in data:
      if is_train:
        processed_x.append(self._process_row(row[:-1]))
        processed_y.append(self._process_output(row[-1]))
      else:
        processed_x.append(self._process_row(row))
    if is_train:
      return np.array(processed_x), np.array(processed_y)
    return np.array(processed_x)

  def _process_row(self, row):
    """
    Processes a single row of the input data

    row:  A raw row from the csv file
    """
    processed_row = []
    for column in NUMERIC_COLUMNS:
      processed_row.append(float(row[column]))
    for column in CATEGORICAL_COLUMNS.keys():
      value = row[column]
      mask = [0] * len(CATEGORICAL_COLUMNS[column])
      mask[CATEGORICAL_COLUMNS[column].index(value)] = 1
      processed_row.extend(mask)
    return processed_row

  def _process_output(self, output):
    return output == 'yes'

