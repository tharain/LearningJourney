/*
 * This class creates a ui that allow user to edit a text file and inset items in alphabetical order and even deletes them.
 */
import java.util.*;

import javax.swing.JFileChooser;

import sun.security.ssl.Debug;

import java.awt.*;
import java.awt.List;
import java.awt.event.*;
import java.io.File;

public class AIConvo extends Frame implements ActionListener, WindowListener{
	private static Storage st;
	
	private static Panel top;
	private static Panel middle;
	private static Panel bottom;
	private static Panel superBottom;
	
	private static List listData;
	private static Label lblPath;
	private static TextField txtInput;
	private static TextField txtOutput;
	private static Button btnPath;
	private static Button btnAdd;
	private static Button btnDelete;
	private static Button btnSave;
	
	//for loading file
	JFileChooser fc;
	
	public AIConvo(){
		setLayout(new FlowLayout());
		initializeControls();
		addListeners();
		initializeFrame();
		initializeThisFrame();
	}
	
	public static void main(String[] args){
		st = new Storage();
		new AIConvo();
	}
	
	private static void initializeDataBase(){
		ArrayList<String> data = st.loadFile();
		listData.removeAll();
		
		for(int i=0; i<data.size(); ++i){
			int index = getIndex(data.get(i));
			
			if(index != -1 && !data.get(i).equals("")){
				listData.add(data.get(i),index);
			}
		}
	}

	private void initializeThisFrame() {
		add(top);
		add(middle);
		add(bottom);
		add(superBottom);
	    setTitle("Text modifier");  // "super" Frame sets its title
	    setSize(450, 500); // "super" Frame sets its initial window size
	    
	    
	    addWindowListener(this);
	    setVisible(true);         // "super" Frame shows
	}

	private void initializeFrame() {
		top = new Panel();
		middle = new Panel();
		bottom = new Panel();
		superBottom = new Panel();
		
		middle.setPreferredSize(new Dimension(600, 320));
		superBottom.setPreferredSize(new Dimension(600, 100));
		top.add(btnPath);
		top.add(lblPath);
		top.add(btnSave);
		middle.add(listData);
		System.out.println(listData.getMinimumSize());
		bottom.add(txtInput);
		superBottom.add(txtOutput);
		superBottom.add(btnAdd);
		superBottom.add(btnDelete);
	}

	private void initializeControls() {
		listData = new List(20);
		lblPath = new Label("No path selected",Label.RIGHT);
		txtInput = new TextField();
		txtInput.setPreferredSize(new Dimension(300,20));
		txtOutput = new TextField();
		txtOutput.setPreferredSize(new Dimension(300,20));
		btnPath = new Button("Open file");
		btnAdd = new Button("Add");
		btnDelete = new Button("Delete");
		btnSave = new Button("Save");
		fc = new JFileChooser();
	}
	
	private void addListeners(){
		btnPath.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                File f = st.getFile();
                if(f!=null){
                	fc.setCurrentDirectory(st.getFile());
                }
                int returnVal = fc.showOpenDialog(AIConvo.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	f = fc.getSelectedFile();
                	st.setFile(f);
                    lblPath.setText(f.getName());
                    initializeDataBase();
                }
            }
        });
		
		btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	if(!txtInput.getText().equals("") && !txtOutput.getText().equals("")){ //the item unique
            		listData.add("Me: " + txtInput.getText());
            		listData.add("AI: " + txtOutput.getText());
            		listData.setPreferredSize(new Dimension(200,900));
            		txtInput.setText("");
            		txtOutput.setText("");
            	}
            }
        });
		
		btnDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	if(listData.getSelectedIndex() != -1){
            		listData.remove(listData.getSelectedIndex());
            	}
            }
        });
		
		btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                File f = st.getFile();
                if(f!=null){
                	fc.setCurrentDirectory(st.getFile());
                }
                int returnVal = fc.showSaveDialog(AIConvo.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	f = fc.getSelectedFile();
                	st.setFile(f);
                    lblPath.setText(f.getName());
                    String[] saveContent = new String[listData.getItemCount()];
                    for(int i = 0; i<saveContent.length; ++i){
                    	saveContent[i] = listData.getItem(i);
                    }
                    st.saveFile(saveContent);
                }
            }
        });
	}
	
	private static int getIndex(String data){
		return listData.getItemCount();
	}

/* -------------------------------------------------------------------------------------------------------------- */
	
	//Invoked when the user attempts to close the window from the window's system menu.
	//The x button
	@Override
	public void windowClosing(WindowEvent e) {
		st.close();
		System.exit(0);
	}
	
	//Invoked the first time a window is made visible.
	@Override
	public void windowOpened(WindowEvent e) {
		
	}
	
	//Invoked when a window has been closed as the result of calling dispose on the window.
	@Override
	public void windowClosed(WindowEvent e) {
		
	}
	
	//Invoked when a window is changed from a normal to a minimized state.
	@Override
	public void windowIconified(WindowEvent e) {
		
	}
	
	//Invoked when a window is changed from a minimized to a normal state.
	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}
	
	//Invoked when the Window is set to be the active Window.
	@Override
	public void windowActivated(WindowEvent e) {
		
	}
	
	//Invoked when a Window is no longer the active Window.
	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
}