import java.util.*;

import javax.swing.JFileChooser;

import sun.security.ssl.Debug;

import java.awt.*;
import java.awt.List;
import java.awt.event.*;
import java.io.File;

public class Aiml extends Frame implements ActionListener, WindowListener{
	private static Storage st;
	private static Panel top;
	private static Panel bottom;
	
	private static Label lblPath;
	private static Label loading;
	private static Button btnPath;
	private static Button btnSave;
	
	private static ArrayList<String> csv;
	
	JFileChooser fc;
	
	public Aiml(){
		setLayout(new FlowLayout());
		initializeControls();
		addListeners();
		initializeFrame();
		initializeThisFrame();
	}
	
	public static void main(String[] args){
		st = new Storage();
		new Aiml();
	}
	
	private void initializeThisFrame() {
		add(top);
		add(bottom);
	    setTitle("aiml to csv convertor");  // "super" Frame sets its title
	    setSize(300, 120); // "super" Frame sets its initial window size
	    
	    addWindowListener(this);
	    setVisible(true);         // "super" Frame shows
	}

	private void initializeFrame() {
		top = new Panel();
		top.setPreferredSize(new Dimension(300, 30));
		bottom = new Panel();

		top.add(btnPath);
		top.add(lblPath);
		top.add(btnSave);
		bottom.add(loading);
	}
	
	private void initializeControls() {
		lblPath = new Label("No path selected", Label.RIGHT);
		loading = new Label("Nope no file loaded here.", Label.CENTER);
		btnPath = new Button("Open file");
		btnSave = new Button("Save");
		fc = new JFileChooser();
	}
	
	// this method attempt to convert .aiml to .aiml.csv
	private static void initializeDataBase()throws Exception{
		loading.setText("currently loading...");
		ArrayList<String> data = st.loadFile();
		csv = new ArrayList<String>();
		
		if(!data.get(0).equals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")){
			throw new Exception();
		}
		
		if(!data.get(1).equals("<aiml version=\"1.0.1\" encoding=\"UTF-8\"?>")){
			throw new Exception();
		}
		
		boolean isCategory = false;
		boolean isPattern = false;
		boolean isTemplate = false;
		boolean isTopic = false;
		
		String fileName = st.getFile().getName();
		String line = "";
		String pattern = "";
		String set = "*,";
		String text = "";
		
		for(int i=2; i<data.size(); ++i){
			String[] fragment = data.get(i).split("\\s+");
			for(int j=0; j<fragment.length; ++j){
				if(isCategory){
					if(fragment[j].equals("</topic>")){
						set = "*,";
						isTopic = false;
					}
					if(fragment[j].equals("</category>")){
						isCategory = false;
						line = line.trim() + "," + fileName;
						//System.out.println(line);
						csv.add(line);
						text = "";
					}else{
						if(isPattern){
							if(fragment[j].equals("</pattern>")){
								isPattern = false;
								line = line + pattern + ",*,";
								pattern = "";
							}else{
								//pattern here
								if(pattern.equals("")){
									pattern = fragment[j];
								}else{
									pattern = pattern + " " + fragment[j];
								}
							}
						}else{
							if(fragment[j].equals("<pattern>")){
								isPattern = true;
							}
							if(isTemplate){
								if(fragment[j].equals("</template>")){
									isTemplate = false;
									line = line + set + text;
								}
							}else{
								if(fragment[j].equals("<template>")){
									isTemplate = true;
									text = "";
								}
							}
						}
					}
					
				}else{
					//the start of a new line
					if(fragment[j].equals("<category>")){
						isCategory = true;
						line = "0,";
					}else if(fragment[j].equals("<topic")){
						j += 1;
						fragment[j] = fragment[j].replaceAll("name=\"", "");
						fragment[j] = fragment[j].replaceAll("\">", "");
						set = fragment[j];
						isTopic = true;
					}else if(isTopic){
						fragment[j] = fragment[j].replaceAll("\">", "");
						set = set + " " + fragment[j];
					}
				}
			}
			if(isTopic){
				isTopic = false;
				set = set + ",";
			}
			if(isTemplate){
				data.set(i, data.get(i).replaceAll("<template>", ""));
				data.set(i, data.get(i).replaceAll("<srai> ", "<srai>"));
				data.set(i, data.get(i).replaceAll(" </srai>", "</srai>"));
				data.set(i, data.get(i).replaceAll(",", "#Comma"));
				text = text + data.get(i).trim();
				text = text.trim();
			}
		}
		loading.setText("done loading...");
	}
	
	private void addListeners(){
		btnPath.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File f = st.getFile();
                if(f!=null){
                	fc.setCurrentDirectory(st.getFile());
                }
                int returnVal = fc.showOpenDialog(Aiml.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	f = fc.getSelectedFile();
                	st.setFile(f);
                    lblPath.setText(f.getName());
                    try{
                    	initializeDataBase();
                    }catch(Exception ee){
                    	loading.setText("Error in conversion. Teminated.");
                    	csv = null;
                    }
                }
            }
        });
		
		btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	fc.setSelectedFile(new File(st.getFile().getName() + ".csv"));
            	File f = st.getFile();
                if(f!=null){
                	fc.setCurrentDirectory(st.getFile());
                }
                int returnVal = fc.showSaveDialog(Aiml.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                	f = fc.getSelectedFile();
                	st.setFile(f);
                    lblPath.setText(f.getName());
                    String[] saveContent = new String[csv.size()];
                    for(int i = 0; i<saveContent.length; ++i){
                    	saveContent[i] = csv.get(i);
                    }
                    st.saveFile(saveContent);
                }
            }
        });
	}
	
/* -------------------------------------------------------------------------------------------------------------- */
	
	//Invoked when the user attempts to close the window from the window's system menu.
	//The x button
	@Override
	public void windowClosing(WindowEvent e) {
		st.close();
		System.exit(0);
	}
	
	//Invoked the first time a window is made visible.
	@Override
	public void windowOpened(WindowEvent e) {
		
	}
	
	//Invoked when a window has been closed as the result of calling dispose on the window.
	@Override
	public void windowClosed(WindowEvent e) {
		
	}
	
	//Invoked when a window is changed from a normal to a minimized state.
	@Override
	public void windowIconified(WindowEvent e) {
		
	}
	
	//Invoked when a window is changed from a minimized to a normal state.
	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}
	
	//Invoked when the Window is set to be the active Window.
	@Override
	public void windowActivated(WindowEvent e) {
		
	}
	
	//Invoked when a Window is no longer the active Window.
	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
}