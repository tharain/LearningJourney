import java.util.*;

public class BinarySearch{
	TreeSet<String> ts;
	
	public BinarySearch(){}
	
	public void setTree(TreeSet<String> bts){
		ts = bts;
	}
	
	public Iterator<String> getData(){
		return  ts.iterator();
	}
}