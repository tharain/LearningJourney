import java.util.*;
import java.io.*;

public class Storage{
	private static File file;
	private static File tempFile;
	private static File hidden;
	
	private BufferedReader hr;
	private BufferedWriter hw;
	
	private BufferedReader input;
	private BufferedOutputStream output;
	
	public Storage(){
		try{
			hidden = new File("ConvoPath.txt");
			if(!hidden.exists()){
				hidden.createNewFile();
			}else{
				hr = new BufferedReader(new FileReader(hidden));
				String pathName = hr.readLine();
				if(isValidFilePath(pathName)){
					tempFile = new File(pathName);
				}
				hr.close();
			}
		}catch(Exception e){}
	}
	
	private boolean isValidFilePath(String pathName) {
		File tempFile = new File(pathName);
		File directory = tempFile.getParentFile();
		return directory.exists();
	}
	
	public void setFile(File f){
		file = f;
		tempFile = f;
		try{
			hw = new BufferedWriter(new FileWriter(hidden));
			hw.write(f.getAbsolutePath());
			hw.close();
		}catch(Exception e){}
	}
	
	public File getFile(){
		return tempFile;
	}
	
	private void initializeFile(){
		try{
			if(!file.exists()){
				file.createNewFile();
			}
		}catch(IOException e){}
	}
	
	public void saveFile(String[] data){
		initializeFile();
		try{
			if(output != null) output.close();
			output = new BufferedOutputStream(new FileOutputStream(file));
			System.out.println(file.getName());
			for(int i=0; i<data.length; ++i){
				data[i] = data[i] + "\n";
				byte[] buffer = data[i].getBytes();
				output.write(buffer, 0, buffer.length);
				output.flush();
			}
		}catch(Exception e){}
	}
	
	public void close(){
		try{
			if(input != null) input.close();
			if(output != null) output.close();
		}catch(Exception e){}
	}
	
	public ArrayList<String> loadFile(){
		try{
			if(input != null) input.close();
			input = new BufferedReader(new FileReader(file));
			ArrayList<String> ts = new ArrayList<String>();
			String str;
			while((str=input.readLine())!=null){
				ts.add(str);
			}
			return ts;
		}catch(Exception e){}
		return null;
	} 
}