import java.util.*;

class Data{
	private final int ODD = 0;
	private final int EVEN = 1;
	private final double[][] EDGE_POTENTIAL = {{1,0.5},{0.5,1}};
	private final double[][] NODE_POTENTIAL = {{0.7,0.3},{0.1,0.9}};
	public Data(){}
	public double edgePotential(int s, int t){
		return EDGE_POTENTIAL[s][t];
	}
	public double[] nodePotential(int s){
		if(s == number(1) || s == number(3) || s == number(5)){
			return NODE_POTENTIAL[ODD];
		}
		return NODE_POTENTIAL[EVEN];
	}
	public int number(int i){
		return i-1;
	}
}
// Represent Belief Propagation information for a single node
class Node{
	private double[] nodePotential;
	private int[] neighbors;
	private HashMap<Integer,double[]> incomingMessage;
	private HashMap<Integer,double[]> outgoingMessage;
	public Node(double[] nodePotential, int[] neighbors){
		this.nodePotential = nodePotential;
		this.neighbors = neighbors;
		this.incomingMessage = new HashMap<Integer,double[]>();
		this.outgoingMessage = new HashMap<Integer,double[]>();
		for(int i=0; i<neighbors.length; ++i){
			int neighbor = neighbors[i];
			setIncomingMessage(neighbor,new double[]{1,1});
			setOutgoingMessage(neighbor,new double[]{1,1});
		}
	}
	public int[] getNeighbors(){
		return neighbors;
	}
	public double[] getNodePotential(){
		return nodePotential;
	}
	public void setIncomingMessage(int i, double[] message){
		this.incomingMessage.remove(i);
		this.incomingMessage.put(i,message);
	}
	public void setOutgoingMessage(int i, double[] message){
		this.outgoingMessage.remove(i);
		this.outgoingMessage.put(i,message);
	}
	public double[] getIncomingMessage(int i){
		return incomingMessage.get(i);
	}
	public double[] getOutgoingMessage(int i){
		return outgoingMessage.get(i);
	}
	//printing of marginal
	public String toString(){
		double[] product = {1,1};
		//multiply with incoming messages
		for(int i=0; i<neighbors.length; ++i){
			int neighbor = neighbors[i];
			double[] neighborNode = getIncomingMessage(neighbor);
			product[0] *= neighborNode[0];
			product[1] *= neighborNode[1];
		}
		//multiple with node potential
		product[0] *= nodePotential[0];
		product[1] *= nodePotential[1];
		double Z = product[0] + product[1];
		//division of Z value
		product[0] /= Z;
		product[1] /= Z;
		return "[ " + product[0] + ", " + product[1] + " ]";
	}
}
// Graph class is for structually plot the graph
class Graph{
	private Node[] nodes = new Node[6];
	private Data data;
	public Graph(Data data){
		//initialize the nodes
		this.data = data;
		nodes[data.number(1)] = new Node( data.nodePotential(data.number(1)),new int[]{data.number(2),data.number(3)});
		nodes[data.number(2)] = new Node( data.nodePotential(data.number(2)),new int[]{data.number(1)});
		nodes[data.number(3)] = new Node( data.nodePotential(data.number(3)),new int[]{data.number(1)});
	}
	/**
	 ** MOST CRITICAL
	 **/
	public void sendMessage(){
		System.out.println("Sending Message...");
		//iterating the nodes
		for(int i=0; i<3; ++i){
			for(int j=0; j<nodes[i].getNeighbors().length; ++j){
				//iterate the nodes i neighbors
				int neighbor = nodes[i].getNeighbors()[j];
				nodes[i].setOutgoingMessage(neighbor,new double[]{0,0});
				//to loop the edge potential
				for(int k=0; k<2; ++k){
					double[] outMsg = nodes[i].getOutgoingMessage(neighbor);
					double[] output = {1,1};
					//product of all other neighbors incoming message
					double result = 1;
					for(int l=0; l<nodes[i].getNeighbors().length; ++l){
						int otherNeighbor = nodes[i].getNeighbors()[l];
						if(otherNeighbor == neighbor) continue;
						result *= nodes[i].getIncomingMessage(otherNeighbor)[k];
					}
					//Step 02: follow formula
					double[] edge = new double[]{data.edgePotential(k,0), data.edgePotential(k,1)};
					System.out.println("k = " + k + ". I am node " + i);
					System.out.println("before output = " + output[0] + " " + output[1]);
					System.out.println("before edge = " + edge[0] + " " + edge[1]);
					System.out.println("before nodepotential = " + nodes[i].getNodePotential()[k]);
					System.out.println("before result = " + result);
					System.out.println("before outmsg = " + outMsg[0] + " " + outMsg[1]);
					System.out.println("apply output * edge * node potential + outMsg");
					output[0] *= edge[0];
					output[1] *= edge[1];
					output[0] *= result;
					output[1] *= result;
					output[0] *= nodes[i].getNodePotential()[k];
					output[1] *= nodes[i].getNodePotential()[k];
					output[0] += outMsg[0];
					output[1] += outMsg[1];
					System.out.println("after output = " + output[0] + " " + output[1]);
					nodes[i].setOutgoingMessage(neighbor,output);
				}
			}
		}
	}
	public void recieveMessaage(){
		for(int i=0; i<3; ++i){
			for(int j=0; j<nodes[i].getNeighbors().length; ++j){
				int neighbor = nodes[i].getNeighbors()[j];
				nodes[i].setIncomingMessage(neighbor, nodes[neighbor].getOutgoingMessage(i));
			}
		}
	}
	public Node getNode(int i){
		return nodes[i];
	}
}
public class SumProduct{
	public static void main(String args[]){
		Data data = new Data();
		Graph graph = new Graph(data);
		for(int i = 0; i < 3; ++i){
			graph.sendMessage();
			graph.recieveMessaage();
		}
		for(int i = 0; i < 3; ++i){
			System.out.println("Single node marginal for node " + (i+1) + ": " + graph.getNode(i));
		}
	}
}